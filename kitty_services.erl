    %%%%% Abstracted version
-module(kitty_services).

-export([start_link/0, order_cat/4, return_cat/2, close_shop/1,
         start_deceased_cat_server/0, show_deceased_cat_list/1, register_deceased_cat/5,
         assign_work/3, retire/2, add_cat/4, browse_cat_with_job/1,
         show_count_all_cat_with_sum_price/1, add_cat_with_price/5,
         start_feed_server/0, show_feed_queue/1, register_cat_to_feed_queue/2, make_cat_hungry/1, feed_the_queue/1,
         hunt_preys/2,
         change_fave_song/3, show_fave_song/1, play_music/2, show_happiness/1,
         register_cat_breed/3, show_registered_breed_cat/1
        ]).
-export([add_cat_with_price_and_age/6,
         show_statistic_of_all_cat_age/1,
         show_statistic_of_all_cat_price/1]).
-export([edit_description_cat/3, edit_description_cat/4, is_edit_by_color/3]).

-export([init/1, handle_call/3, handle_cast/2, tukar_kucing/4]).
-export([cat_rambut_kucing/3]).
-export ([start_doctor_server/0, start_clinic_server/0, make_doctor/2, make_clinic/2, register_clinic/3,
          register_doctor/3,assign_doctor/4,show_list/1]).
-export([cari_kucing/2, search_cat_h/3]).
-export([kucing_termurah/1, cheapest_cat/2]).
-export([show_cleaning_service_price/1, cat_cleaning_service/3]).
-export([change_working_hour/3]).
-export([prediksi_kematian/2]).
-export([make_sing_cat/3]).
-export([beauty_contest/3]).
-export([discount_by_name/3,check_discount_by_name/3]).
-export([add_promo/5, count_cashback/4, search_cat_with_price/2, start_promo_server/0]).
-export([adopt_cat/3]).
-export([train_cat/2]).

-export([tambah_kucing_warna/4, lihat_warna_kucing/1, show_cat/1, remove_duplicates/1]).
-export([add_cat_with_medal/5, cat_vs_cat/3]).
-export([rename_cat/3]).
-export([cat_give_birth/5]).
-export([add_cat_centre/2, add_attraction/3, start_cat_centre_server/0, show_attractions/2]).
-export([add_cat_with_gender/5, make_cat_with_gender/4, get_married_cat/3, married/2]).

-export([level_cat_up/3, get_all_rpg/1]).
-export([add_playing_cat/2, show_bored_cat_list/1, play_with_cat/2]).
-export([start_veterinarian_server/0, register_vet/3, register_cat_to_vet/4]).
-export([sterilize_cat/2, show_sterilized_cat_list/1]).
-export([add_cat_with_birth/5, get_cat_month_birthday/2]).
-export([nama_korea_kucing/2, penamaan_korea_kucing/2]).
-export([start_nekomimi/0, register_emp/4, open_cafe/4, show_stats/1]).

-record(cat, {name, color=black, description, sterilized=false, hungry=false,
 job=unemployed, level=1, hp=1, atk=1, def=1, birthdate=date(),
 fave_song=mars_fasilkom, is_happy=false, gender=male, breed=unknown,
 intelligence=stupid, medal=0}).

-record(deceased_cat, {name, date, cause}).
-record(doctor, {name, specialization={}}).
-record(clinic, {name, max_doctor=5, doctor_list=[]}).
-record(cat_with_price, {name, color=black, description, price=0}).
-record(cat_with_price_and_age, {name, color=black, description, price=0, age=0}).
-record(playing_cat, {name, last_play=erlang:timestamp()}).
-record(veterinarian, {name, location, patients=[]}).
-record(cat_with_parent, {name, color=black, description, parent}).
-record(cat_centre, {name, attractions=[], cats=[]}).
-record(promo, {code, minimum_price, percentage, maximum_cashback}).
-record(cat_skill, {name,chef_skill,wait_skill}).

%%% Client API
start_link() -> my_server:start_link(?MODULE, []).
start_deceased_cat_server() -> my_server:start_link(?MODULE, []).

start_veterinarian_server() -> my_server:start_link(?MODULE, []).

start_doctor_server() -> my_server:start_link(?MODULE, []).
start_clinic_server() -> my_server:start_link(?MODULE, []).
start_feed_server() -> my_server:start_link(?MODULE, []).

start_cat_centre_server() -> my_server:start_link(?MODULE, []).
start_promo_server() -> my_server:start_link(?MODULE, []).
start_nekomimi() -> my_server:start_link_hehe(?MODULE,{[],0,0}).

%% Synchronous call
order_cat(Pid, Name, Color, Description) ->
    my_server:call(Pid, {order, Name, Color, Description}).

add_cat_with_medal(Pid, Name, Color, Description, Medal)->
    my_server:call(Pid, {add_with_medal, Name, Color, Description, Medal}).

register_emp(Pid,Name,ChefSk,WaitSk) -> my_server:call(Pid,{register_emp,Name,ChefSk,WaitSk}).

open_cafe(Pid,ChefList,WaitList,Cust) -> my_server:call(Pid,{open_cafe,ChefList,WaitList,Cust}).

show_stats(Pid) -> my_server:call(Pid,show_stats).

%%Print list of record
show_list(Pid)->
    my_server:call(Pid,{print}).

%Assign doctor ke klinik
assign_doctor(Pid1,Pid2,SearchedDoctor,SearchedClinic) ->
    ResultDoctor = my_server:call(Pid1,{filter_doctor,SearchedDoctor}),
    ResultClinic = my_server:call(Pid2,{filter_clinic,SearchedClinic}),
    if ResultDoctor =/= [] andalso ResultClinic =/= [] ->
            {_,ClinicName,_,_} = hd(ResultClinic),
            my_server:call(Pid2,{assign,true,ClinicName,hd(ResultDoctor)});
       true ->
            my_server:call(Pid2,{assign,false,null,null})
    end.
beauty_contest(Pid, Theme, Colorcode) -> my_server:call(Pid, {beauty_contest, Theme, Colorcode}).

add_cat_with_price(Pid, Name, Color, Description, Price) ->
    my_server:call(Pid, {add_with_price, Name, Color, Description, Price}).

show_count_all_cat_with_sum_price(Pid) ->
    my_server:call(Pid, show_count_all_cat_with_sum_price).

show_deceased_cat_list(Pid) -> my_server:call(Pid, show_deceased).

show_feed_queue(Pid) -> my_server:call(Pid, show_feed_queue).

make_cat_hungry(Cat = #cat{}) -> #cat{name=Cat#cat.name, color=Cat#cat.color, description=Cat#cat.description, hungry=true}.

feed_the_queue(Pid) -> my_server:call(Pid, feed_queue).

cat_vs_cat(Pid, Cat1, Cat2) ->
    my_server:call(Pid, {cat_vs_cat, Cat1, Cat2}).

level_cat_up(Pid, Name, Method) -> my_server:call(Pid, {level_up, Name, Method}).

get_all_rpg(Pid) -> my_server:call(Pid, get_all_rpg).
hunt_preys(Pid, Preys) ->
    my_server:call(Pid, {hunt_preys, Preys}).
adopt_cat(Pid, Name, Owner) ->
    my_server:call(Pid, {adopt, Name, Owner}).

add_cat_with_price_and_age(Pid, Name, Color, Description, Price, Age) ->
    my_server:call(Pid, {add_with_price_and_age, Name, Color, Description, Price, Age}).

add_cat_with_gender(Pid, Name, Color, Description, Gender) ->
	my_server:call(Pid, {add_with_gender, Name, Color, Description, Gender}).

show_statistic_of_all_cat_price(Pid) ->
    my_server:call(Pid, {show_statistic_of_all_cat_price}).

show_statistic_of_all_cat_age(Pid) ->
    my_server:call(Pid, {show_statistic_of_all_cat_age}).


kucing_termurah(Pid) ->
    my_server:call(Pid, {kucing_termurah}).


edit_description_cat(Pid, Name, NewDescription) ->
    my_server:call(Pid, {edit_description, Name, undefined, NewDescription}).

edit_description_cat(Pid, Name, Color, NewDescription) ->
    my_server:call(Pid, {edit_description, Name, Color, NewDescription}).

train_cat(Pid, Name) ->
    my_server:call(Pid, {train_cat, Name}).

cat_give_birth(Pid, Name, Color, Description, Parent) ->
    my_server:call(Pid, {cat_give_birth, Name, Color, Description, Parent}).

%%Melakukan registrasi dokter ke server
register_doctor(Pid, Name, Specialization)->
    my_server:cast(Pid, {register_doctor, my_server:call(Pid,{new_doctor,Name,Specialization})}).

%%Melakukan registrasi klinik ke server
register_clinic(Pid, Name, Max_Doctor)->
    my_server:cast(Pid, {register_clinic, my_server:call(Pid,{new_clinic,Name,Max_Doctor})}).

%% Register doctor to server
%register_doctor(Pid, Doctor = #doctor{}) ->
%    my_server:cast(Pid, {register_doctor, Doctor}).
%% Register clinic to server
%register_clinic(Pid, Clinic = #clinic{}) ->
%    my_server:cast(Pid, {register_clinic, Clinic}).
make_sing_cat(Pid, Name, Note) ->
    my_server:call(Pid, {make_sing_cat, Name, Note}).

to_string([]) -> "";
to_string([H|T]) -> " " ++ H ++ to_string(T).

%% Tests the notes that the cat sing
%% Cheatsheet: A A# B C C# D D# E F F# G G# A
make_sing_helper(Note) ->
    NoteInChar = hd(lists:concat(["", Note])),
    if %% an E Note
        NoteInChar >= 101 ->
            MajorNote1 = NoteInChar - 4;
        true ->
            MajorNote1 = NoteInChar + 3
    end,

    if  %% a G Note
        MajorNote1 >= 103 ->
            MajorNote2 = MajorNote1 - 6;
        true ->
            MajorNote2 = MajorNote1 + 1
    end,

    %% The notes are B or F
    case NoteInChar of
        98 -> %% B Note %%
            [[NoteInChar], [MajorNote1], [MajorNote2] ++ "#"];
        102 -> %% F Note %%
            [[MajorNote1-1] ++ "#", [MajorNote2]];
        _ ->
            [[NoteInChar], [MajorNote1], [MajorNote2]]
    end.
get_married_cat(Pid, Cat1, Cat2) ->
	my_server:call(Pid, {get_married, Cat1, Cat2}).
register_cat_breed(Pid, Name, NewBreed) -> my_server:call(Pid, {register_breed, Name, NewBreed}).

show_registered_breed_cat(Pid) -> my_server:call(Pid, show_breed_cat).

return_cat(Pid, Cat = #cat{}) ->
    my_server:cast(Pid, {return, Cat}).

register_deceased_cat(Pid1, Pid2, Name, Date = {DD, MM, YY}, Cause) ->
    case calendar:valid_date({YY, MM, DD}) of
        true ->
            my_server:cast(Pid1, {decease, Name}),
            my_server:call(Pid2, {decease, Name, Date, Cause});
        false ->
            my_server:call(Pid1, invalid_date)
    end.

register_cat_to_feed_queue(Pid, Cat = #cat{}) ->
    case Cat#cat.hungry of
        true ->
            my_server:cast(Pid, {add_feed_queue, Cat}),
            my_server:call(Pid, {add_feed_queue, Cat});
        false ->
            my_server:call(Pid, {cat_not_hungry, Cat})
    end.

%% Call to change server's working hour
change_working_hour(Pid, Start, End) when is_integer(Start), is_integer(End)->
    if End =< Start    ->
            erlang:error("Start begins before End");
         End > Start     ->
            my_server:change_working_hour(Pid, Start, End)
    end.

is_edit_by_color(Name, Color, Cats) ->
    if Color =:= undefined ->
        lists:filter(fun(Cat) -> Cat#cat.name =:= Name end, Cats);
    true ->
        lists:filter(fun(Cat) -> ((Cat#cat.name =:= Name) and (Cat#cat.color =:= Color)) end, Cats)
    end.

%% Synchronous call
close_shop(Pid) ->
    my_server:call(Pid, terminate).

%% Synchronous call untuk fungsi tukar_kucing
tukar_kucing(Pid, Name, Color, Description) ->
    my_server:call(Pid, {tukar, Name, Color, Description}).

% Synchronous call untuk fungsi cat_rambut_kucing
cat_rambut_kucing(Pid, Name, NewColor) ->
    my_server:call(Pid, {cat_rambut, Name, NewColor}).

%% Synchronous call untuk fungsi cari_kucing
%% Format pencarian [{with/without, category, query}, {with/without, category, query}, ... ]
%%
%% Memiliki mode pencarian AND, jadi query1 AND query2 AND query3
%%
%% Sedangkan {with/without, category, query} dianggap satu kueri.
%%      with menandakan bahwa hasil harus sama dengan kueri, without menandakan bahwa hasil harus tidak sama dengan kueri
%%      category menyatakan kategori pencarian berdasarkan record: name, color, atau description
%%          (If you want to add another category, add method search_cat_h on private method section)
%%      query adalah hal yang ingin dicari
%%
cari_kucing(Pid, Query) ->
    my_server:call(Pid, {search_cat, Query}).

%% Synchronous call untuk fungsi assign_work
assign_work(Pid, Name, Job) ->
    my_server:call(Pid, {work, Name, Job}).

%% Synchronous call untuk fungsi retire
retire(Pid, Name) ->
    my_server:call(Pid, {retire, Name}).

discount_by_name(Pid, Name, Discount) ->
	my_server:call(Pid, {discount_name, Name, Discount}).

check_discount_by_name(Pid, Name, Discount) ->
	my_server:call(Pid, {check_discount_name, Name, Discount}).

%% Synchronous call untuk fungsi browse_cat_with_job
browse_cat_with_job(Pid) ->
    my_server:call(Pid, browse).

%% Synchronous call untuk fungsi add_cat
add_cat(Pid, Name, Color, Description) ->
    my_server:call(Pid, {add_cat, Name, Color, Description}).

add_cat_with_birth(Pid, Name, Color, Description, Birth) ->
    my_server:call(Pid, {add_cat_birth, Name, Color, Description, Birth}).

get_cat_month_birthday(Pid, Bulan) ->
    my_server:call(Pid, {get_cat_birth_month, Bulan}).

show_cleaning_service_price(Pid) ->
      my_server:call(Pid, show_cleaning_price).

cat_cleaning_service(Pid, Cat = #cat{}, Money) ->
      my_server:call(Pid, {cleaning, Cat, Money}).

%% Synchronous call untuk menambahkan kucing yang warnanya sama
tambah_kucing_warna(Pid, Name, Color, Description) ->
    my_server:call(Pid, {color_register, Name, Color, Description}).

%% Synchronous call untuk menampilkan seluruh kucing yang ada di server
show_cat(Pid) ->
    my_server:call(Pid, show).

%% Synchronous call untuk menampilkan seluruh warna yang ada di server
lihat_warna_kucing(Pid) ->
    my_server:call(Pid, colors).

%% Synchronous call untuk mengganti nama kucing
rename_cat(Pid, CurrentName, NewName) ->
    my_server:call(Pid, {rename, CurrentName, NewName}).

add_playing_cat(Pid, Name) ->
    my_server:call(Pid, {add_playing_cat, Name}).

show_bored_cat_list(Pid) ->
    my_server:call(Pid, show_bored).

play_with_cat(Pid, Name) ->
    my_server:call(Pid, {play, Name}).
%% Synchronous call for adding a new veterinarian
register_vet(Pid, Name, Location) ->
    my_server:call(Pid, {register_vet, Name, Location}).

%% Synchronous call for adding a new patient to a veterinarian
%% where Pid1=Pid for cat server process, Pid2=Pid for vet server process
register_cat_to_vet(Pid1, Pid2, CatName, VetName) ->
    Cat = cari_kucing(Pid1, [{with, name, CatName}]),
    my_server:call(Pid2, {register_cat_to_vet, Cat, VetName}).
%% Synchronous call untuk steril kucing
sterilize_cat(Pid, Name) ->
    my_server:call(Pid, {sterilize_cat, Name}).

%% Synchronous call untuk menampilkan kucing yang telah disterilkan
show_sterilized_cat_list(Pid) ->
    my_server:call(Pid, show_sterilized_cat_list).

%% Synchronous call untuk menampilkan prediksi tanggal kematian kucing yang ada di server
prediksi_kematian(Pid, Name) ->
    my_server:call(Pid, {predict, Name}).
%% Synchronous call untuk mengubah lagu kesukaan kucing
change_fave_song(Pid, Name, FaveSong) ->
    my_server:call(Pid, {fave_song, Name, FaveSong}).

%% Synchronous call untuk menampilkan kucing dan lagu kesukaannya
show_fave_song(Pid) ->
    my_server:call(Pid, show_fave_song).

%% Synchronous call untuk memainkan lagu untuk kucing-kucing
play_music(Pid, Song) ->
    my_server:call(Pid, {play_music, Song}).

%% Synchronous call untuk menampilkan status kebahagiaan kucing
show_happiness(Pid) ->
    my_server:call(Pid, show_happiness).
%% Synchronous call untuk fungsi add cat_centre and attraction
%% dimana dalam sebuah cat centre mempunyai beberapa attraction contohnya gym, theater, library, etc
add_attraction(Pid, Name, Attractions) ->
    my_server:call(Pid, {add_attraction, Name, [Attractions]}).

add_cat_centre(Pid, {Name}) ->
    my_server:call(Pid, {add_cat_centre, Name, []});

add_cat_centre(Pid, {Name, Attractions}) ->
    my_server:call(Pid, {add_cat_centre, Name, [Attractions]}).

show_attractions(Pid, Name) ->
    my_server:call(Pid, {show_attractions, Name}).
% Synchronous call untuk menambahkan promo
add_promo(Pid, Code, MinimumPrice, Percentage, MaximumCashback) ->
    my_server:call(Pid, {add_promo, Code, MinimumPrice, Percentage, MaximumCashback}).

count_cashback(PidPromos, PidCats, Code, CatName) ->
    Out = search_cat_with_price(PidCats, CatName),
    my_server:call(PidPromos, {count_cashback, Out, Code}).

search_cat_with_price(Pid, CatName) ->
    my_server:call(Pid, {search_cat_with_price, CatName}).
%% Synchronous call untuk menentukan nama korea kucing berdasarkan tahun lahir
nama_korea_kucing(Pid, Name) ->
    my_server:call(Pid, {korean_name, Name}).

%%% Server functions
init(State) -> State. %% no treatment of info here!

remove_duplicates(List) ->
    lists:foldl(fun(Color, Set) ->
            Length = length(lists:filter(fun(Other) -> Other =:= Color end, Set)),
            case Length of
                0 -> Set ++ [Color];
                _ -> Set
            end
        end,
    [hd(List)], [hd(List) | tl(List)]).

handle_call({cat_give_birth, Name, Color, Description, Parent}, From, Cats) ->
    Hasil = search_cat_h([{with, name, Parent}], Cats, []),
    if Hasil =/= [] ->
            Anak = make_cat_with_parent(Name, Color, Description, Parent),
            my_server:reply(From, {ok, "kucing berhasil melahirkan dan memiliki anak."}),
            Cats ++ [Anak];
       Hasil =:= [] ->
          my_server:reply(From, {error, "kucing tidak ditemukan."}),
        Cats
    end;

%% Server function untuk mengubah lagu kesukaan kucing
handle_call({fave_song, Name, FaveSong}, From, Cats) ->
    case lists:filter(fun(Cat) -> Cat#cat.name == Name end, Cats) of
        [] ->
            my_server:reply(From, {error, "no cat with respective name"}),
            Cats;
        _ ->
            my_server:reply(From, {ok, changed}),
            lists:map(
                fun(Cat) ->
                    case Cat#cat.name == Name of
                        true -> make_cat_with_music(Cat#cat.name, Cat#cat.color, Cat#cat.description, Cat#cat.job, FaveSong, Cat#cat.is_happy);
                        false -> Cat
                    end
                end,
                Cats
                )
    end;

%% Server function untuk menampilkan menampilkan kucing dan lagu kesukaannya
handle_call(show_fave_song, From, Cats) ->
    my_server:reply(From, {ok, [{Cat#cat.name, Cat#cat.fave_song} || Cat <- Cats]}),
    Cats;

%% Server function untuk memainkan lagu untuk kucing-kucing
handle_call({play_music, Song}, From, Cats) ->
    case lists:filter(fun(Cat) -> Cat#cat.fave_song == Song end, Cats) of
        [] ->
            my_server:reply(From, {error, "no cat loves that song"}),
            Cats;
        _ ->
            my_server:reply(From, {ok, played}),
            lists:map(
                fun(Cat) ->
                    case Cat#cat.fave_song == Song of
                        true -> make_cat_with_music(Cat#cat.name, Cat#cat.color, Cat#cat.description, Cat#cat.job, Cat#cat.fave_song, true);
                        false -> Cat
                    end
                end,
                Cats
                )
    end;

%% Server function untuk untuk menampilkan status kebahagiaan kucing
handle_call(show_happiness, From, Cats) ->
    my_server:reply(From, {ok, [{Cat#cat.name, Cat#cat.is_happy} || Cat <- Cats]}),
    Cats;

handle_call({order, Name, Color, Description}, From, Cats) ->
    Hasil = search_cat_h([{with,name,Name},{with,color,Color},{with,description,Description}], Cats, []),
    if Hasil =:= [] ->
            my_server:reply(From, make_cat(Name, Color, Description)),
            Cats;
       Hasil =/= [] ->
          my_server:reply(From, hd(Hasil)),
            [C || C <- Cats, C =/= hd(Hasil)]
    end;
    % This line of code does not do what it needs to do, consider deleting
    % if Cats =:= [] ->
    %     my_server:reply(From, make_cat(Name, Color, Description)),
    %     Cats;
    %    Cats =/= [] ->
    %     my_server:reply(From, hd(Cats)),
    %     tl(Cats)
    % end;

handle_call({beauty_contest, Theme, Colorcode}, From, Cats) ->
  case Cats of
    [] -> my_server:reply(From, {ok, "Tidak ada kucing yang jadi peserta."});
    _ -> [Winner, ParticiCats] = lists:foldl(
      fun(Cat, Compo) ->
        [TempWinnerCat, FailedCats] = Compo,
        case TempWinnerCat of
          none -> if is_record(Cat, cat_with_price_and_age) -> [Cat, FailedCats];
                    true -> [none, FailedCats ++ [Cat]]
                  end;
          _ ->
            if is_record(Cat, cat_with_price_and_age) -> bc_battle(TempWinnerCat, Cat, FailedCats, Colorcode, Theme);
              true -> [TempWinnerCat, FailedCats ++ [Cat]]
            end
        end
      end, [none, []], Cats),
      case Winner of
        none -> my_server:reply(From, {ok, "Tidak ada peserta yang lolos."}),
          Cats;
        _ -> FinalWinnercat = bc_winner_process(Winner, Theme, Colorcode),
          my_server:reply(From, {
            ok, lists:flatten(io_lib:format(
              "Pemenang Beauty Contest tahun ini adalah ~p. Selamat! Sekarang sudah berumur ~p tahun dan memiliki nilai jual sebesar ~p rupiah.",
              [list_to_atom(FinalWinnercat#cat_with_price_and_age.name), FinalWinnercat#cat_with_price_and_age.age, FinalWinnercat#cat_with_price_and_age.price]))
          }),
          ParticiCats ++ [FinalWinnercat]
      end
  end;

handle_call({add_with_price, Name, Color, Description, Price}, From, Cats) ->
    if Price >= 0 ->
        my_server:reply(From, {ok, "success add cat"}),
        Cats ++ [make_cat_with_price(Name, Color, Description, Price)];

    Price < 0 ->
        my_server:reply(From, {error, "can't negative price"}),
    Cats
   end;

handle_call({cat_vs_cat, Cat1, Cat2}, From, Cats) ->
    TuanRumah = [C || C <- Cats, C#cat.name =:= Cat1],
    Penantang = [C || C <- Cats, C#cat.name =:= Cat2],
    if Cat1 =:= Cat2 ->
        my_server:reply(From, {error, "Nama petarung tidak boleh sama"}),
        Cats;
    TuanRumah =:= [] ->
        R= io_lib:format("~p",[Cat1]),
        Temp = "Tidak ada nama "++lists:flatten(R)++" di server",
        my_server:reply(From, {error, Temp}),
        Cats;
    Penantang =:= [] ->
        R= io_lib:format("~p",[Cat2]),
        Temp = "Tidak ada nama "++lists:flatten(R)++" di server",
        my_server:reply(From, {error, Temp}),
        Cats;
    true ->
        Pemenang = duel(hd(TuanRumah), hd(Penantang)),
        Nama = Pemenang#cat.name,
        Medal = Pemenang#cat.medal + 1,
        PemenangBaru = Pemenang#cat{medal = Medal},
        R= io_lib:format("~p",[Nama]),
        Temp = "Kucing "++lists:flatten(R)++" Menang!!",
        my_server:reply(From, {ok, Temp}),
        [PemenangBaru| lists:delete(Pemenang, Cats)]
    end;

handle_call({level_up, Name, atk}, From, Cats) ->
    Temp = lists:filter(fun(Cat) -> Cat#cat.name == Name end, Cats),
    case Temp of
        [] ->
            my_server:reply(From, {error, "Cats Not Found"}),
            Cats;
        _ ->
            Temp2 = lists:map(fun(Cat) -> Cat#cat{level = Cat#cat.level + 1, atk= Cat#cat.atk + 1} end, Temp),
            Temp3 = lists:filter(fun (Elem) -> not lists:member(Elem, Temp) end, Cats ),
            my_server:reply(From, {ok, "yay level up, atk up"}),
            Temp2 ++ Temp3
    end;

handle_call({level_up, Name, def}, From, Cats) ->
    Temp = lists:filter(fun(Cat) -> Cat#cat.name == Name end, Cats),
    case Temp of
        [] ->
            my_server:reply(From, {error, "Cats Not Found"}),
            Cats;
        _ ->
            Temp2 = lists:map(fun(Cat) -> Cat#cat{level = Cat#cat.level + 1, def= Cat#cat.def + 1} end, Temp),
            Temp3 = lists:filter(fun (Elem) -> not lists:member(Elem, Temp) end, Cats ),
            my_server:reply(From, {ok, "yay level up, def up"}),
            Temp2 ++ Temp3
    end;

handle_call({level_up, Name, hp}, From, Cats) ->
    Temp = lists:filter(fun(Cat) -> Cat#cat.name == Name end, Cats),
    case Temp of
        [] ->
            my_server:reply(From, {error, "Cats Not Found"}),
            Cats;
        _ ->
            Temp2 = lists:map(fun(Cat) -> Cat#cat{level = Cat#cat.level + 1, hp= Cat#cat.hp + 1} end, Temp),
            Temp3 = lists:filter(fun (Elem) -> not lists:member(Elem, Temp) end, Cats ),
            my_server:reply(From, {ok, "yay level up, hp up"}),
            Temp2 ++ Temp3
    end;

handle_call({level_up, _, _}, From, Cats) ->
    my_server:reply(From, {error, "Method not allowed!!! (choose between atk, def, hp)"}),
    Cats;


handle_call(get_all_rpg, From, Cats) ->
    my_server:reply(From, {ok,"Format : name,level,atk,def,hp", [{Cat#cat.name, Cat#cat.level, Cat#cat.atk, Cat#cat.def, Cat#cat.hp} || Cat <- Cats]}),
    Cats;

%%%Menampilkan jumlah uang yang harus dibayar untuk membeli semua kucing, beserta jumlah kucingnya
handle_call(show_count_all_cat_with_sum_price, From, Cats) ->
    Total = lists:foldl(fun(_, Sum) -> Sum + 1 end, 0, Cats),
    List_price = [Cat#cat_with_price.price|| Cat <- Cats],
    Sum = lists:sum(List_price),
    my_server:reply(From,{Sum,Total}),
    Cats;

%%Melakukan filtering doktor berdasarkan nama
handle_call({filter_doctor,Name}, From, Doctors) ->
    Filter = lists:filter(fun(Doctor)-> Doctor#doctor.name == Name end, Doctors),
    my_server:reply(From,Filter), Doctors;
%%Melakukan filtering klinik berdasarkan nama
handle_call({filter_clinic,Name}, From, Clinics) ->
    Filter = lists:filter(fun(Clinic)-> Clinic#clinic.name == Name end, Clinics),
    my_server:reply(From,Filter), Clinics;

%%Mengembalikan record clinic yang telah dibuat
handle_call({new_clinic,Name,Max_Doctor}, From, Clinics) ->
    my_server:reply(From, make_clinic(Name, Max_Doctor)), Clinics;
%%Mengembalikan record doctor yang telah dibuat
handle_call({new_doctor,Name,Specialization}, From, Doctors) ->
    my_server:reply(From, make_doctor(Name, Specialization)), Doctors;

%%Server function untuk assign dokter ke klinik
handle_call({assign,Result,Search,Updated},From,Obj)->
    if Result =:= false ->
        my_server:reply(From, {error,"Either Doctor or Clinic not exist"}), Obj;
       true ->
        Update_Result = update_record(Obj,Search,Updated,[],no),
        {UpdatedResult,Duplicate} = Update_Result,
        if Duplicate == yes ->
            my_server:reply(From, {error,"Duplicate. Assign another doctor"});
           [Obj] == [UpdatedResult] ->
            my_server:reply(From, {error,"Already at maximum limit"});
           true->
            my_server:reply(From, {ok,"Both exist"})
        end, UpdatedResult
    end;

%%Server function untuk mengeprint record dalam server
handle_call({print}, From, Obj) ->
    my_server:reply(From, ok), print_list(Obj);

%%%% Server functions untuk layanan tukar kucing
handle_call({tukar, Name, Color, Description}, From, Cats) ->
    if Cats =:= [] ->
        my_server:reply(From, {error, "Tidak ada kucing untuk ditukar."}),
        Cats;
       Cats =/= [] ->
        my_server:reply(From, hd(Cats)),
        [make_cat(Name, Color, Description)] ++ tl(Cats)

    end;

%%%% Server functions untuk layanan cat rambut kucing
handle_call({cat_rambut, Name, NewColor}, From, Cats) ->
    % Kalau tidak ada kucing pada Cats
    if Cats =:= [] ->
            my_server:reply(From, {error, "Tidak ada kucing untuk di cat rambutnya."}),
            Cats;
    % Kalau ada kucing
    true ->
        case lists:filter(fun(Cat) -> Cat#cat.name =:= Name end, Cats) of
            % Kalau tidak ada kucing dengan nama Name
            [] ->
                my_server:reply(From, {error, "Tidak ada kucing dengan nama tersebut"}),
                Cats;
            % Kalau ada satu
            _ ->
                Cat = lists:filter(fun(Cat) -> Cat#cat.name =:= Name end, Cats),
                CurrCat = hd(Cat),
                NewCat = CurrCat#cat{color = NewColor},
                NewCats = Cats -- [CurrCat],
                case Cat of
                    % Kalau kucing dengan nama Name hanya satu
                    [_] ->
                        my_server:reply(From, {ok, "Kucing berhasil di-cat"});
                    % Kalau kucing dengan nama Name lebih dari satu
                    _ ->
                        my_server:reply(From, {ok, "Terdapat lebih dari 1 kucing dengan nama tersebut, 1 kucing terdepan pada list berhasil di-cat"})
                end,
                NewCats ++ [NewCat]
        end
    end;
%% Handle call untuk nama korea kucing
handle_call({korean_name, Name},From,Cats) ->
    if Cats =:= [] ->
        my_server:reply(From, {error, "Kucing tak ada"}),
        Cats;
    Cats =/= [] ->
        Umur_kucing = [Cat#cat_with_price_and_age.age || Cat <- Cats, Cat#cat_with_price_and_age.name =:= Name],
        Nama_korea = penamaan_korea_kucing(Name, Umur_kucing),
        my_server:reply(From, Nama_korea),
        Cats
    end;

%% Handle call untuk cari_kucing
handle_call({search_cat, Query}, From, Cats) ->
    Results = search_cat_h(Query, Cats, []),
    if Results =:= [] ->
        my_server:reply(From, {error, "Tidak ditemukan kucing yang sesuai dengan kueri"}),
        Cats;
       Results =:= [error] ->
        my_server:reply(From, {error, "Sintaks pencarian salah"}),
        Cats;
       Results =/= [] ->
        my_server:reply(From, {ok, Results}), Cats
    end;

%% Handle call untuk pencarian kucing termurah
handle_call({kucing_termurah}, From, Cats) ->
    Results = cheapest_cat(Cats, []),
    if Results =:= [] ->
         my_server:reply(From, {error, "Tidak ada kucing yang tersedia"}),
         Cats;
        Results =/= [] ->
         my_server:reply(From, {ok, Results}), Cats
    end;


handle_call(terminate, From, Cats) ->
    my_server:reply(From, ok),
    terminate(Cats);

handle_call(show_deceased, From, Cats) ->
    Total = lists:foldl(fun(_, Sum) -> Sum + 1 end, 0, Cats),
    my_server:reply(From,
                    {[{Cat#deceased_cat.name, Cat#deceased_cat.date, Cat#deceased_cat.cause} || Cat <- Cats],
                    Total}),
    Cats;


%%%% Server functions untuk layanan memberikan discount kepada kucing dengan input nama dan persen yang ingin diberikan
handle_call({discount_name, Name, Discount}, From, Cats) ->
	if Discount =< 0 ->
		my_server:reply(From, {error, "Diskon harus lebih besar dari 0%"}),
		Cats;
	Discount > 0 ->
		CatResults = lists:filter(fun(Cat) -> Cat#cat_with_price.name =:= Name end, Cats),
		if  CatResults =/= [] ->
			TheCat = hd(CatResults),
			ThePrice = TheCat#cat_with_price.price,
			CalculateDiscount = (100 - Discount) / 100 * ThePrice,
			Hiks = TheCat#cat_with_price{price = CalculateDiscount},
			my_server:reply(From, {ok, "Discount berhasil diberikan :>"}),
			[Hiks| lists:delete(TheCat, Cats)];
		CatResults =:= [] ->
			my_server:reply(From, {error, "Tidak bisa memberikan diskon karena kucing dengan nama tersebut tidak ada"})
		end
	end;


%%%% Server functions untuk mengecek berapakah harga kucing apabila akan diberikan discount
handle_call({check_discount_name, Name, Discount}, From, Cats) ->
	if Discount =< 0 ->
		my_server:reply(From, {error, "Diskon harus lebih besar dari 0%"}),
		Cats;
	Discount > 0 ->
		CatResults = lists:filter(fun(Cat) -> Cat#cat_with_price.name =:= Name end, Cats),
		if  CatResults =/= [] ->
			TheCat = hd(CatResults),
			ThePrice = TheCat#cat_with_price.price,
			CalculateDiscount = (100 - Discount) / 100 * ThePrice,
			my_server:reply(From, {ok, CalculateDiscount}),
			Cats;
		CatResults =:= [] ->
			my_server:reply(From, {error, "Tidak bisa mengecek diskon karena kucing dengan nama tersebut tidak ada"})
		end
	end;


handle_call(invalid_date, From, Cats) ->
    my_server: reply(From, {error, invalid_date}),
    Cats;

handle_call({decease, Name, Date, Cause}, From, DeceasedCats) ->
    case lists:filter(fun(Cat) -> Cat#deceased_cat.name == Name end, DeceasedCats) of
        [] ->
            my_server:reply(From, {ok, noted}),
            DeceasedCats ++ [make_deceased_cat(Name, Date, Cause)];
        _ ->
            my_server:reply(From, {error, "can't die more than once"}),
            DeceasedCats
    end;

% handle_call({search_cat, Query}, From, Cats) ->
%     Results = search_cat_h(Query, Cats, []),
%     if Results =:= [] ->
%         my_server:reply(From, {error, "Tidak ditemukan kucing yang sesuai dengan kueri"}),
%         Cats;
%        Results =:= [error] ->
%         my_server:reply(From, {error, "Sintaks pencarian salah"}),
%         Cats;
%        Results =/= [] ->
%         my_server:reply(From, {ok, Results}), Cats
%     end;

%% Handle call untuk membuat suara Kucing
handle_call({make_sing_cat, Name, Note}, From, Cats) ->
    case search_cat_h([{with,name,Name}], Cats, []) of
        [] ->
            my_server:reply(From, {error, "Sorry, no cat found with the following name"}), Cats;
        _ ->
            NoteTesting = string:to_upper(to_string(make_sing_helper(Note))),
            Reply = lists:concat([Name, " sings in", NoteTesting, " major notes."]),
            my_server:reply(From, {ok, Reply}), Cats
    end;

%%%% Server functions for assigning work to respective cat
handle_call({work, Name, Job}, From, Cats) ->
    case lists:filter(fun(Cat) -> Cat#cat.name == Name end, Cats) of
        [] ->
            my_server:reply(From, {error, "no cat with respective name"}),
            Cats;
        _ ->
            my_server:reply(From, {ok, assigned}),
            lists:map(
                fun(Cat) ->
                    case Cat#cat.name == Name of
                        true -> make_cat_with_job(Cat#cat.name, Cat#cat.color, Cat#cat.description, Job);
                        false -> Cat
                    end
                end,
                Cats
                )
    end;

%%%% Server functions for retiring a respective cat
handle_call({retire, Name}, From, Cats) ->
    case lists:filter(fun(Cat) -> Cat#cat.name == Name end, Cats) of
        [] ->
            my_server:reply(From, {error, "no cat with respective name"}),
            Cats;
        _ ->
            my_server:reply(From, {ok, retired}),
            lists:map(
                fun(Cat) ->
                    case Cat#cat.name == Name of
                        true -> make_cat_with_job(Cat#cat.name, Cat#cat.color, Cat#cat.description, retirement);
                        false -> Cat
                    end
                end,
                Cats
                )
    end;

%%%% Server functions for showing all cats
handle_call(browse, From, Cats) ->
    my_server:reply(From, {ok, [{Cat#cat.name, Cat#cat.job} || Cat <- Cats]}),
    Cats;

handle_call({add_cat, Name, Color, Description}, From, Cats) ->
    my_server:reply(From, {ok, add_cat}),
    Cats ++ [make_cat(Name, Color, Description)];

handle_call({edit_description, Name, Color, NewDescription}, From, Cats) ->
    case is_edit_by_color(Name, Color, Cats) of
        [] ->
            my_server:reply(From, {error, "can't find any cat"}),
            Cats;
        [_]  ->
            Get_Cat = hd(is_edit_by_color(Name, Color, Cats)),
            EditedCat = Get_Cat#cat{description = NewDescription},
            my_server:reply(From, {ok, "description updated"}),
            [EditedCat| lists:delete(Get_Cat, Cats)];
        _   ->
            my_server:reply(From, {error, "more than 1 cat detected, please specify the name and color"}),
            Cats
    end;

handle_call({cleaning, Cat = #cat{}, Money}, From, Cats) ->
    Cost = (length(Cats) * 5000) + 20000,
    if
        Cost > Money ->
            my_server:reply(From, {error, "Uang anda tidak cukup untuk pelayanan pembersihan kucing."}),
            Cats;
        true ->
            Change = Money - Cost,
            Name = Cat#cat.name,
            Message = lists:concat([Name, " bersih kembali, kembalian anda ", Change]),
            my_server:reply(From, {ok, Message}),
            Cats
    end;

handle_call(show_cleaning_price, From, Cats) ->
      Cost = (length(Cats) * 5000) + 20000,
      Message = lists:concat(["Harga jasa pembersihan kucing saat ini adalah ", Cost]),
      my_server:reply(From, {Message}),
    Cats;

handle_call({adopt, Name, Owner}, From, Cats) ->
    Search = search_cat_h([{with, name, Name}], Cats, []),

    if Search =/= [] ->
            Message = lists:concat(["Congrats ", Owner,"! You have adopted a cat named ", Name, "."]),
            my_server:reply(From, {ok, Message}),

            CurrCats = [C || C <- Cats, C#cat.name =/= Name],
            CurrCats;

        Search =:= [] ->
            my_server:reply(From, {error, "Sorry, cat with that name can not be found"}),
            Cats
    end;

handle_call({add_feed_queue, Cat = #cat{}}, From, Queue) ->
    my_server:reply(From, {ok, add_feed_queue, {Cat#cat.name, Cat#cat.color, Cat#cat.description, Cat#cat.hungry}, queue_length, length(Queue)}),
    Queue;

handle_call(show_feed_queue, From, Queue) ->
    my_server:reply(From, {[{Cat#cat.name, Cat#cat.color, Cat#cat.description, Cat#cat.hungry} || Cat <- Queue]}),
    Queue;

handle_call({cat_not_hungry, Cat = #cat{}}, From, Queue) ->
    my_server:reply(From, {error, cat_not_hungry, {Cat#cat.name, Cat#cat.color, Cat#cat.description, Cat#cat.hungry}}),
    Queue;

handle_call({add_with_price_and_age, Name, Color, Description, Price, Age}, From, Cats) ->
    if
        Price < 0 ->
            my_server:reply(From, {ok, "Price cannot be negative number"}),
            Cats;
        Age < 0 ->
            my_server:reply(From, {ok, "Age cannot be negative number"}),
            Cats;
        true ->
            my_server:reply(From, {ok, cat_with_price_and_age}),
            Cats ++ [make_cat_with_price_and_age(Name, Color, Description, Price, Age)]
    end;

%%% Server function untuk handle add cat with gender
handle_call({add_with_gender, Name, Color, Description, Gender}, From, Cats) ->
	if  Gender =:= "male" ->
			my_server:reply(From, {ok, "success add male cat"}),
			Cats ++ [make_cat_with_gender(Name, Color, Description, Gender)];
		Gender =:= "female" ->
			my_server:reply(From, {ok, "success add female cat"}),
			Cats ++ [make_cat_with_gender(Name, Color, Description, Gender)];
		true ->
			my_server:reply(From, {error, "invalid gender type, please enter beetween Female or Male"}),
			Cats
	end;
%%% Menikahkan Cat1 dengan Cat2 dan melahirkan anak dari Cat yang Female
%%% Melahirkan Cat menggunakan fungsi cat_give_birth yang dibuat mahasiswa lain
handle_call({get_married, Cat1, Cat2}, From, Cats) ->
		First = search_cat_h([{with, name, Cat1}], Cats, []),
		Last = search_cat_h([{with, name, Cat2}], Cats, []),
		if  First =:= [] ->
				my_server:reply(From, {error, "kucing tidak ditemukan."}),
				Cats;
			Last =:= [] ->
				my_server:reply(From, {error, "kucing tidak ditemukan."}),
				Cats;
			true ->
				case married(First, Last) of
					true ->
						my_server:reply(From, {ok, "Married success"}),
						Cats;
					false ->
						my_server:reply(From, {error, "Cannot married cat with same gender"}),
						Cats
				end
		end;

handle_call({show_statistic_of_all_cat_age}, From, Cats) ->
    Stat = dict:new(),
    StatWFreq = dict:store(frequency, count_age_freq(Cats), Stat),
    StatWMean = dict:store(mean, count_mean(dict:fetch(frequency, StatWFreq), 0, 0), StatWFreq),
    StatWMode = dict:store(mode, get_mode(dict:fetch(frequency, StatWFreq)), StatWMean),
    my_server:reply(From, {ok, dict:to_list(StatWMode)}),
    Cats;

handle_call({show_statistic_of_all_cat_price}, From, Cats) ->
    Stat = dict:new(),
    StatWFreq = dict:store(frequency, count_price_freq(Cats), Stat),
    StatWMean = dict:store(mean, count_mean(dict:fetch(frequency, StatWFreq), 0, 0), StatWFreq),
    StatWMode = dict:store(mode, get_mode(dict:fetch(frequency, StatWFreq)), StatWMean),
    my_server:reply(From, {ok, dict:to_list(StatWMode)}),
    Cats;

handle_call(show, From, Cats) ->
    my_server:reply(From, {Cats}),
    Cats;

handle_call(colors, From, Cats) ->
    if Cats =:= [] ->
        my_server:reply(From, {error, "No cats found"}),
        Cats;
       Cats =/= [] ->
        Colors = lists:map(fun(Cat) -> Cat#cat.color end, Cats),
        my_server:reply(From, {ok, remove_duplicates(Colors)}),
        Cats
    end;

handle_call({color_register, Name, Color, Description}, From, Cats) ->
    if  Cats =:= [] ->
            my_server:reply(From, {ok, "added cat"}),
            Cats ++ [make_cat(Name, Color, Description)];
        Cats =/= [] ->
            AllColor = remove_duplicates([Cat#cat.color || Cat <- Cats]),
            case lists:member(Color, AllColor) of
                true ->
                    my_server:reply(From, {ok, "added cat with the same color"}),
                    Cats ++ [make_cat(Name, Color, Description)];
                false ->
                    my_server:reply(From, {error, "Please add cat with available colors:", AllColor}),
                    Cats
            end
    end;

handle_call({add_with_medal, Name, Color, Description, Medal}, From, Cats) ->
    Is_integer = is_integer(Medal),
    if Is_integer ->
         if 
            Medal >= 0 ->
                my_server:reply(From, {ok, "success add cat"}),
                Cats ++ [make_cat_with_medal(Name, Color, Description, Medal)];
            Medal < 0 ->
                my_server:reply(From, {error, "Medal is in integer"}),
                Cats
         end;
      true -> 
        my_server:reply(From, {error, "Write medal in integer"}),
        Cats
   end;

handle_call({train_cat, Name}, From, Cats) ->
    Selected_cat_list = lists:filter(fun(Cat) -> Cat#cat.name == Name end, Cats),
    if
        Selected_cat_list == [] -> my_server:reply(From, {error, "There's no cat with that name!"}), Cats;
        true ->
            Selected_cat = hd(Selected_cat_list),
            Get_intel = Selected_cat#cat.intelligence,
            Randomize_train = rand:uniform(),
            if
                Get_intel == stupid andalso Randomize_train > 0.7->
                        New_cat = Selected_cat#cat{intelligence = average},
                        my_server:reply(From, {ok, "The cat's intelligence is rising, it's now average"}),
                        [New_cat| lists:delete(Selected_cat, Cats)];
                Get_intel == average andalso Randomize_train > 0.4 ->
                        New_cat = Selected_cat#cat{intelligence = smart},
                        my_server:reply(From, {ok, "The cat's intelligence is rising, it's now smart"}),
                        [New_cat| lists:delete(Selected_cat, Cats)];
                Get_intel == smart ->
                    my_server:reply(From, {error, "The cat's intelligence is on max level"}),
                    Cats;
                true ->
                    my_server:reply(From, {error, "The cat's doesn't want to train, please try again"}),
                    Cats
            end
    end;
    
handle_call({rename, CurrentName, NewName}, From, Cats) ->
    if CurrentName =:= NewName ->
        my_server:reply(From, "Cannot rename with same name"), Cats;
    true ->
        case lists:filter(fun(Cat) -> Cat#cat.name == NewName end, Cats) of
        [] ->
            case lists:filter(fun(Cat) -> Cat#cat.name == CurrentName end, Cats) of
            [] ->
                my_server:reply(From, {error, "Cat with name " ++ CurrentName ++ " not exists"}), Cats;
            _ ->
                my_server:reply(From, {ok, "Cat rename success"}),
                lists:map(
                fun(Cat) ->
                    case Cat#cat.name == CurrentName of
                        true -> make_cat(NewName, Cat#cat.color, Cat#cat.description);
                        false -> Cat
                    end
                end, Cats)
            end;
        _ -> my_server:reply(From, {error, "Cat with name " ++ NewName ++ " already exists"}), Cats
        end
    end;

handle_call({register_vet, Name, Location}, From, Vets) ->
    my_server:reply(From, {ok, add_vet}),
    Vets ++ [register_new_vet(Name, Location)];

handle_call({register_cat_to_vet, Cat, VetName}, From, Vets) ->
    case Cat of
        {error, _} -> my_server:reply(From, {error, "No cat with that name"}), Vets;
        {ok, L} ->
            %%% Diasumsikan cat yang ingin didaftarkan ke dokter adalah kucing pertama di list
            Patient = hd(L),
            case lists:filter(fun(Vet) -> Vet#veterinarian.name =:= VetName end, Vets) of
                [] ->
                    my_server:reply(From, {error, "No veterinarian with that name"}),
                    Vets;
                [Vet|_] ->
                    Patients = Vet#veterinarian.patients,
                    VetWithPatient = Vet#veterinarian{patients=[Patient|Patients]},
                    my_server:reply(From, {ok, VetWithPatient}),
                    [VetWithPatient|lists:delete(Vet, Vets)]
            end
    end;

handle_call({add_cat_birth, Name, Color, Description, Birth}, From, Cats) when is_list(Birth)->
    L = string:tokens(Birth, "-"),
    [Hari, Bulan, Tahun] = lists:map(fun(Z) -> list_to_integer(Z) end, L),
    %% Validasi input
    Valid = calendar:valid_date(Tahun, Bulan, Hari) and ({Tahun, Bulan, Hari} =< date()),
    case Valid of
        true ->
            my_server:reply(From, {ok, "Cat with birth date added"}),
            Cats ++ [make_cat_with_birth(Name, Color, Description, {Tahun, Bulan, Hari})];
        false ->
            my_server:reply(From, {error, "Please enter with a valid birth date"}),
            Cats
    end;

handle_call({get_cat_birth_month, Bulan}, From, Cats) when (is_integer(Bulan) and (Bulan >=1) and (Bulan =< 12)) ->
    ListCat = lists:filter(fun(Cat) ->
                {_, BulanCat, _} = Cat#cat.birthdate,
                    if BulanCat =:= Bulan -> true;
                       BulanCat =/= Bulan -> false
                    end
                end, Cats),
    my_server:reply(From, {ok, [ {Cat#cat.name, Cat#cat.birthdate} || Cat <- ListCat]}),
    Cats;

handle_call(feed_queue, From, []) ->
    my_server:reply(From, {error, empty_feed_queue, all_cat_is_happy}),
    [];

handle_call({sterilize_cat, Name}, From, Cats) ->
    % mencari kucing dengan nama #Name yang belum steril
    Results = [Cat || Cat <- Cats, Cat#cat.name =:= Name, Cat#cat.sterilized =:= false],
    if Results =:= [] ->
        my_server:reply(From, {error, "Kucing tidak ditemukan."}),
        Cats;
       Results =/= [] ->
           case Results of
               [_] ->
                   my_server:reply(From, {ok, "Kucing berhasil disterilkan"});
               _ ->
                   my_server:reply(From, {ok, "Ditemukan lebih dari 1 kucing, kucing pertama yang akan disterilkan"})
               end,
               Cat = hd(Results),
               SterilizedCat = Cat#cat{sterilized = true},
               NewCats = [SterilizedCat | lists:delete(Cat, Cats)],
               my_server:reply(From, {ok, "kucing berhasil disterilkan"}),
               NewCats
    end;

handle_call(show_sterilized_cat_list, From, Cats) ->
    Results = lists:filter(fun(Cat) -> Cat#cat.sterilized =:= true end, Cats),
    if Results =:= [] ->
          my_server:reply(From, {error, "Kucing tidak ditemukan."}),
          Cats;
       Results =/= [] ->
          my_server:reply(From, {ok, "Daftar kucing steril", Results}),
          Cats
    end;

handle_call({predict, Name}, From, Cats) ->
    if Cats =:= [] ->
          my_server:reply(From, {error, "Sepertinya tidak ada kucing yang tersedia :)"}),
          Cats;
       Cats =/= [] ->
          Cat_age = [Cat#cat_with_price_and_age.age || Cat <- Cats, Cat#cat_with_price_and_age.name =:= Name],
          Message = predict_cat_age(Name, Cat_age),
          my_server:reply(From, Message),
          Cats
    end;

handle_call(feed_queue, From, [Cat|Queue]) ->
    my_server:reply(From, {ok, finish_eat, {Cat#cat.name, Cat#cat.color, Cat#cat.description, Cat#cat.hungry}, happy_cat}),
    Queue;

handle_call({add_playing_cat, Name}, From, Cats) ->
    my_server:reply(From, {ok, "Cat succesfully added"}),
    Cats ++ [make_playing_cat(Name)];

handle_call(show_bored, From, Cats) ->
    Current = erlang:timestamp(),
    my_server:reply(From,
                    [Cat#playing_cat.name || Cat <- Cats, is_cat_bored(Cat, Current)]),
    Cats;

handle_call({play, Name}, From, Cats) ->
    PlayCat = [Cat || Cat <- Cats, Cat#playing_cat.name =:= Name],
    NotPlayCat = [Cat || Cat <- Cats, Cat#playing_cat.name =/= Name],
    AfterPlay = [make_playing_cat(Cat#playing_cat.name) || Cat <- PlayCat],
    my_server:reply(From, {ok, length(AfterPlay)}),
    NotPlayCat ++ AfterPlay;

handle_call({hunt_preys, Preys}, From, Cats) ->
    HunterCats = lists:filter(fun(Cat) -> Cat#cat.job =:= hunter end, Cats),
    case HunterCats of
        [] ->
            my_server:reply(From, {error, "Please add cats with 'hunter' job first!"});
        _  ->
            PreysCaptors = lists:map(fun(Prey) -> {Prey, random_filter([Cat#cat.name || Cat <- HunterCats], 0.25)} end, Preys),
            CaughtPreys = lists:filter(fun({_, Captors}) -> length(Captors) >= 1 end, PreysCaptors),
            case CaughtPreys of
                [] ->
                    my_server:reply(From, {ok, "Failed to capture any preys"});
                _  ->
                    my_server:reply(From, {ok, CaughtPreys})
            end
    end,
    Cats;

%%handle call untuk cat_centre
handle_call({add_cat_centre, Name, Attractions}, From, Centres) ->
    case lists:filter(fun(Centre) -> Centre#cat_centre.name == Name end, Centres) of
      [_] -> my_server:reply(From, {error, "Cat Centre's already registered"}),
            Centres;
      [] -> my_server:reply(From, {ok, add_cat_centre}),
          Centres ++ [make_cat_centre(Name, Attractions,[])]
    end;

handle_call({add_attraction, Name, Attractions}, From, Centres) ->
    case lists:filter(fun(Centre) -> Centre#cat_centre.name == Name end, Centres) of
      [] -> my_server:reply(From, {error, "Cat Centre's not registered"}),
            Centres;
      [H|_] ->
            case lists:filter(fun(Att) -> Att == hd(Attractions) end, H#cat_centre.attractions) of
              [_] -> my_server:reply(From, {error, "The attractions is already exist"}),
                    Centres;
              [] ->
                H2 = H#cat_centre{attractions = H#cat_centre.attractions ++ Attractions},
                my_server:reply(From, {ok, add_attraction}),
                lists:delete(H, Centres) ++ [H2]
            end
    end;

handle_call({show_attractions, Name}, From, Centres) ->
    case lists:filter(fun(Centre) -> Centre#cat_centre.name == Name end, Centres) of
      [] -> my_server:reply(From, {error, "Cat Centre's not registered"}),
            Centres;
      [H|_] -> my_server:reply(From, {ok, H#cat_centre.attractions}),
            Centres
    end;

handle_call({add_promo, Code, MinimumPrice, Percentage, MaximumCashback}, From, Promos) ->
    case lists:keymember(Code, 2, Promos) of
        true ->
            my_server:reply(From, {error, "Promo Code Already Exist"}), Promos;
        false ->
            case (MinimumPrice < 0) or (Percentage < 0) or (MaximumCashback < 0) of
                true -> my_server:reply(From, {error, "Minimum Price Or Percentage Or Maximum Cash Back Can't Be Negative"}),
                    Promos;
                false -> my_server:reply(From, {ok, "Promo Code Added"}),
                    Promos ++ [make_promo(Code, MinimumPrice, Percentage, MaximumCashback)]
            end
    end;

handle_call({count_cashback, {Msg, Cat}, Code}, From, Promos) ->
    case Msg of
        error -> my_server:reply(From, {error, Cat}), Promos;
        ok -> my_server:reply(From, check_cashback(Cat, Code, Promos)), Promos
    end;

handle_call({search_cat_with_price, CatName}, From, Cats) ->
    Cat = [C || C <- Cats, C#cat_with_price.name == CatName],
    case Cat of
        [] -> my_server:reply(From, {error, "Cat Not Found"}), Cats;
        [H|_] -> my_server:reply(From, {ok, H}), Cats
    end;

handle_call({add_with_breed, Col, Desc, Name, Breed}, From, Cats) ->
    my_server:reply(From, {ok, register_cat_breed}),
    Cats ++ [make_cat_with_breed(Col, Desc, Name, Breed)];

handle_call(show_breed_cat, From, Cats) ->
    my_server:reply(From, {[{Cat#cat.name, Cat#cat.breed} || Cat <- Cats, Cat#cat.breed =/= unknown]}),
    Cats;

handle_call({register_breed, Name, NewBreed}, From, Cats) ->
    Results = lists:filter(fun(Cat) -> Cat#cat.name =:= Name end, Cats),
    case Results of
        [] ->
             my_server:reply(From, {error, "Ups, there's no cat with that name."}),
             Cats;
        _  ->
            UnknownCat = lists:filter(fun(Cat) -> Cat#cat.breed == unknown end, Results),
            case UnknownCat of
                [_] ->
                    Caaat = hd(UnknownCat),
                    Changed = Caaat#cat{breed = NewBreed},
                    NewCats = [Changed| lists:delete(Caaat, Cats)],
                    case UnknownCat of
                        [_] ->
                            my_server:reply(From, {ok, "We have noted that cat's breed."});
                        _ ->
                            my_server:reply(From, {ok, "There are so many cat with that name, we have noted the first cat's breed."})
                    end,
                    NewCats;
                _ ->
                    my_server:reply(From, {error, "Ups, all cat with that name has been registered."}),
                    Cats
            end
    end;

handle_call(show_stats,From,{[],Rating,Rev}) ->
	my_server:reply(From, {ok, "No employee", ratingAsterisk(Rating), Rev}),
	{[],Rating,Rev};

handle_call(show_stats,From,{Emp,Rating,Rev}) ->
	List = [{Cat#cat_skill.name, Cat#cat_skill.chef_skill, Cat#cat_skill.wait_skill} || Cat <- Emp],
	my_server:reply(From, {ok,List,ratingAsterisk(Rating),Rev}),
	{Emp,Rating,Rev};
	
handle_call({register_emp,Name,ChefSk,WaitSk},From,{Emp,Rating,Rev}) ->
	List = filter(fun(Cat) -> Cat#cat_skill.name == Name end, Emp),
	case List of
		[] ->
			my_server:reply(From, {ok,Name,"registered"}),
			{Emp++[make_employee(Name,ChefSk,WaitSk)], Rating, Rev};
		_ ->
			my_server:reply(From,{error,Name,"already registered"}),
			{Emp,Rating,Rev}
	end;

handle_call({open_cafe,ChefList,WaitList,Cust},From,{Emp,Rating,Rev}) ->
	ListName = [Cat#cat_skill.name || Cat <- Emp],
	CheckList = check(ChefList++WaitList, ListName),
	case CheckList of
		[] ->
			ChefRecord = [Cat || Cat <- Emp, Elem <- ChefList, Cat#cat_skill.name == Elem],
			WaitRecord = [Cat || Cat <- Emp, Elem <- WaitList, Cat#cat_skill.name == Elem],
			ChefSum = fold(fun(X,Acc) -> (X#cat_skill.chef_skill + Acc) end, ChefRecord, 0),
			WaitSum = fold(fun(X,Acc) -> (X#cat_skill.wait_skill + Acc) end, WaitRecord, 0),
			EmpSum = ChefSum + WaitSum,
			RevNow = Rating * Cust + EmpSum,
			NewRev = Rev + RevNow,
			NewRating = EmpSum div 50,
			my_server:reply(From,{ok,ratingAsterisk(NewRating),RevNow}),
			{Emp,NewRating,NewRev};
		_ ->
			my_server:reply(From, {error,CheckList,"unregistered or duplicate employee"}),
			{Emp,Rating,Rev}
	end.

handle_cast({return, Cat = #cat{}}, Cats) -> [Cat|Cats];
%%Menambahkan record clinic ke server
handle_cast({register_clinic, Clinic = #clinic{}}, Clinics) -> [Clinic|Clinics];
%%Menambahkan record doctor ke server
handle_cast({register_doctor, Doctor = #doctor{}}, Doctors) -> [Doctor|Doctors];

handle_cast({decease, Name}, Cats) ->
    lists:filter(fun(Cat) -> Cat#cat.name =/= Name end, Cats);

handle_cast({add_feed_queue, Cat = #cat{}}, Queue) -> Queue ++ [Cat].

%%% Private functions

duel(Cat1, Cat2) ->
    hd([X||{_,X} <- lists:sort([ {rand:uniform(), N} || N <- [Cat1, Cat2]])]).

make_cat(Name, Col, Desc) ->
    #cat{name=Name, color=Col, description=Desc}.

make_cat_with_medal(Name, Col, Desc, Medal) ->
    #cat{name=Name, color=Col, description=Desc, medal=Medal}.

make_deceased_cat(Name, Date, Cause) ->
    #deceased_cat{name=Name, date=Date, cause=Cause}.

%%Membuat record clinic
make_clinic(Name, Max_Doctor) -> #clinic{name=Name,max_doctor=Max_Doctor,doctor_list=[]}.
%%Membuat record doctor
make_doctor(Name, Specialization) -> #doctor{name=Name,specialization=Specialization}.
%%Mengeluarkan output item ke layar
print_list(List) -> [io:format("~p ~n",[Item]) || Item <- List], List.

%%Fungsi untuk melakukan update record terhadap suatu elemen dalam record dan mengembalikan
%%List klinik terbaru yang sudah diupdate
update_record([],_,_,Result,Duplicate)->{Result,Duplicate};
update_record(List,Searched,Updated,Result,Duplicate)->
    Head = hd(List),
    {_,DocName,_} = Updated,
    CheckDocName = lists:filter(fun({doctor,Name,_})->Name =:= DocName end, Head#clinic.doctor_list),
    DoctorNum = lists:foldl(fun(_,B)->B+1 end,0,Head#clinic.doctor_list),
    if Head#clinic.name =:= Searched andalso DoctorNum =/= Head#clinic.max_doctor andalso [Updated] =/= CheckDocName->
        UpdatedDoctorList = Head#clinic.doctor_list++[Updated],
        NewHead = Head#clinic{doctor_list = UpdatedDoctorList},
        update_record(tl(List),Searched,Updated,Result++[NewHead],Duplicate);
       [Updated] == CheckDocName ->
        update_record(tl(List),Searched,Updated,Result++[Head],yes);
       true->
        update_record(tl(List),Searched,Updated,Result++[Head],Duplicate)
    end.

make_cat_with_price(Name, Col, Desc, Price) ->
    #cat_with_price{name=Name, color=Col, description=Desc, price=Price}.

make_cat_with_job(Name, Col, Desc, Job) ->
    #cat{name=Name, color=Col, description=Desc, job=Job}.

make_cat_with_price_and_age(Name, Color, Description, Price, Age) ->
    #cat_with_price_and_age{name=Name, color=Color, description=Description, price=Price, age=Age}.

make_playing_cat(Name) ->
    #playing_cat{name=Name, last_play=erlang:timestamp()}.
make_cat_with_parent(Name, Color, Description, Parent) ->
    #cat_with_parent{name=Name, color=Color, description=Description, parent=Parent}.
make_cat_with_birth(Name, Color, Description, Birth) ->
    #cat{name=Name, color=Color, description=Description, birthdate=Birth}.
make_cat_with_music(Name, Col, Desc, Job, FaveSong, IsHappy) ->
    #cat{name=Name, color=Col, description=Desc, job=Job, fave_song=FaveSong, is_happy=IsHappy}.
make_cat_centre(Name, Attractions, Cats) ->
    #cat_centre{name=Name, attractions=Attractions, cats=Cats}.
make_cat_with_gender(Name, Color, Description, Gender) ->
	#cat{name=Name, color=Color, description=Description, gender=Gender}.
penamaan_korea_kucing(Nama, Umur) ->
    if Umur =:= [] ->
        {error, lists:concat([Nama, " tidak ada pada record"])};
    true ->
        Umur_cat = hd(Umur),
        Current = calendar:local_time(),
        {{Tahun,_,_},_} = Current,
        Tahun_ini = Tahun,
        Tahun_lahir = Tahun_ini - Umur_cat,
        Angka_terakhir = Tahun_lahir rem 10,
        case Angka_terakhir of
            0 -> Hasil_nama = "Park";
            1 -> Hasil_nama = "Kim";
            2 -> Hasil_nama = "Shin";
            3 -> Hasil_nama = "Choi";
            4 -> Hasil_nama = "Song";
            5 -> Hasil_nama = "Kang";
            6 -> Hasil_nama = "Han";
            7 -> Hasil_nama = "Lee";
            8 -> Hasil_nama = "Sung";
            9 -> Hasil_nama = "Jung"
        end,
    {ok, lists:concat(["Nama kucing ",Nama," memiliki nama korea ",Hasil_nama])}
    end.
make_cat_with_breed(Name, Col, Desc, Breed) ->
    #cat{name=Name, color=Col, description=Desc, breed=Breed}.

%%% Util functions
random_filter(List, Probability) ->
    lists:filter(fun (_) -> rand:uniform() =< Probability end, List).

count_age_freq(Cats) ->
    lists:foldl(fun (C, D) -> orddict:update_counter(C#cat_with_price_and_age.age, 1, D) end, orddict:new(), Cats).

count_price_freq(Cats) ->
    lists:foldl(fun (C, D) -> orddict:update_counter(C#cat_with_price_and_age.price, 1, D) end, orddict:new(), Cats).

get_mode([]) -> none;
get_mode(L) -> get_mode(L, 0, 0).
get_mode([], Mode, _) -> Mode;
get_mode([{N, F}|T], _, MaxFreq) when F > MaxFreq -> get_mode(T, N, F);
get_mode([{_, F}|T], Mode, MaxFreq) when F =< MaxFreq -> get_mode(T, Mode, MaxFreq).

count_mean([], C, Sum) -> Sum/C;
count_mean([{N, F}|T], C, Sum) -> count_mean(T, C + F, Sum + (N * F)).

is_cat_bored(Cat, Current) ->
    {MegaCur, SecCur, _} = Current,
    {MegaCat, SecCat, _} = Cat#playing_cat.last_play,
    Diff = 1000000 * (MegaCur - MegaCat) + SecCur - SecCat,
    if Diff >= 300 ->
        true;
       true ->
        false
    end.

terminate(Cats) ->
    [io:format("~p was set free.~n",[C#cat.name]) || C <- Cats],
    exit(normal).

%% Private Function for cheapest_cat
cheapest_cat(Cat1, Results) when Cat1 =:= [] -> Results;
cheapest_cat(Cat1, Results) when Results =:= [] -> cheapest_cat((tl(Cat1)), [hd(Cat1)]);

cheapest_cat(Cat1, Results) when (hd(Results))#cat_with_price.price < (hd(Cat1))#cat_with_price.price -> cheapest_cat((tl(Cat1)), Results);
cheapest_cat(Cat1, Results) when (hd(Results))#cat_with_price.price =:= (hd(Cat1))#cat_with_price.price -> cheapest_cat((tl(Cat1)), Results ++ [hd(Cat1)]);
cheapest_cat(Cat1, Results) when (hd(Results))#cat_with_price.price > (hd(Cat1))#cat_with_price.price -> cheapest_cat((tl(Cat1)), [hd(Cat1)]).

%% Private Beauty Contest (BC) Utilities
bc_pre(Value, Criteria, Multiplier) -> string:str(
  lists:reverse(string:to_lower(lists:flatten(io_lib:format("~p", [Value])))), lists:reverse(string:to_lower(Criteria))) * Multiplier.

bc_battle(TempWinnerCat, Cat, Cats, Color, Desc) ->
  TempRank = bc_pre(TempWinnerCat#cat_with_price_and_age.color, Color, 1) +
    bc_pre(TempWinnerCat#cat_with_price_and_age.description, Desc, 1) + length(TempWinnerCat#cat_with_price_and_age.description),
  CatRank = bc_pre(Cat#cat_with_price_and_age.color, Color, 1) +
    bc_pre(Cat#cat_with_price_and_age.description, Desc, 1) + length(Cat#cat_with_price_and_age.description),
  if CatRank > TempRank -> [Cat, Cats ++ [TempWinnerCat]];
    true -> [TempWinnerCat, Cats ++ [Cat#cat_with_price_and_age{age = Cat#cat_with_price_and_age.age + 1}]]
  end.

bc_winner_process(Cat, Theme, Colorcode) -> Cat#cat_with_price_and_age{
  price = trunc(Cat#cat_with_price_and_age.price * 2), age = Cat#cat_with_price_and_age.age + 1,
  description = Cat#cat_with_price_and_age.description ++ ", Winner of Beauty Contest (" ++ Theme ++ Colorcode ++ ")"}.

%% Private Function for search_cat
search_cat_h([], _, Results) -> Results;

search_cat_h([{with, name, Query}|T], Cats, Results) ->
    if Results =:= [] -> search_cat_h(T, Cats, [C || C <- Cats, C#cat.name =:= Query]);
       Results =/= [] -> search_cat_h(T, Cats, [C || C <- Results, C#cat.name =:= Query]) end;
search_cat_h([{without, name, Query}|T], Cats, Results) ->
    if Results =:= [] -> search_cat_h(T, Cats, [C || C <- Cats, C#cat.name =/= Query]);
       Results =/= [] -> search_cat_h(T, Cats, [C || C <- Results, C#cat.name =/= Query]) end;

search_cat_h([{with, color, Query}|T], Cats, Results) ->
    if Results =:= [] -> search_cat_h(T, Cats, [C || C <- Cats, C#cat.color =:= Query]);
       Results =/= [] -> search_cat_h(T, Cats, [C || C <- Results, C#cat.color =:= Query]) end;
search_cat_h([{without, color, Query}|T], Cats, Results) ->
    if Results =:= [] -> search_cat_h(T, Cats, [C || C <- Cats, C#cat.color =/= Query]);
       Results =/= [] -> search_cat_h(T, Cats, [C || C <- Results, C#cat.color =/= Query]) end;

search_cat_h([{with, description, Query}|T], Cats, Results) ->
    if Results =:= [] -> search_cat_h(T, Cats, [C || C <- Cats, C#cat.description =:= Query]);
       Results =/= [] -> search_cat_h(T, Cats, [C || C <- Results, C#cat.description =:= Query]) end;
search_cat_h([{without, description, Query}|T], Cats, Results) ->
    if Results =:= [] -> search_cat_h(T, Cats, [C || C <- Cats, C#cat.description =/= Query]);
       Results =/= [] -> search_cat_h(T, Cats, [C || C <- Results, C#cat.description =/= Query]) end;

search_cat_h(_, _, _) -> [error].

register_new_vet(Name, Location) ->
    #veterinarian{name=Name, location=Location, patients=[]}.

predict_cat_age(Name, Cat_age) ->
    if Cat_age =:= [] ->
        {error, lists:concat([Name, " tidak ada di dalam record!"])};
    true ->
        Age = hd(Cat_age) * 365,
        Current_date = calendar:date_to_gregorian_days(erlang:date()),
        Birth_date = Current_date - Age,
        Predicted_age = round((4 + rand:uniform()) * 365),
        if Age > Predicted_age ->
            {ok, lists:concat(["Selamat, umur ", Name," telah melewati prediksi umur kematian :)"])};
        true ->
            {Year, Month, Day} = calendar:gregorian_days_to_date(Birth_date + Predicted_age),
            {ok, lists:concat(["Prediksi kematian ", Name," : Tanggal = ", Day, ", Bulan = ", Month, ", Tahun = ", Year])}
        end
    end.
%%% Private function for married

married([FirstCat|T], [SecondCat|T]) ->
	if  FirstCat#cat.gender =:= "male" ->
		if SecondCat#cat.gender =:= "female" -> true;
		   true -> false
		end;
	    FirstCat#cat.gender =:= "female" ->
	    if SecondCat#cat.gender =:= "male" -> true;
	    	true -> false
	    end
	end.
make_promo(Code, MinimumPrice, Percentage, MaximumCashback) ->
    #promo{code=Code, minimum_price=MinimumPrice, percentage=Percentage, maximum_cashback=MaximumCashback}.

check_cashback({_,_,_,_,Price}, Code, Promos) ->
    case lists:keymember(Code, 2, Promos) of
        false -> {error, "Promo Code Invalid"};
        true -> [P|_] = get_promo(Promos, Code), total_cashback(P, Price)
    end.

get_promo(Promos, Code) -> [Promo || Promo <- Promos, Promo#promo.code == Code].

total_cashback({_,_,MinimumPrice, Percentage, MaximumCashback}, Price) ->
    case MinimumPrice > Price of
        true -> {error, "Promo code doesn't meet minimum price"};
        false ->
            Cashback = erlang:trunc(Price * Percentage / 100),
            case Cashback > MaximumCashback of
                true -> {ok, MaximumCashback};
                false -> {ok, Cashback}
            end
    end.

make_employee(Name,ChefSk,WaitSk) -> #cat_skill{name=Name, chef_skill=ChefSk, wait_skill=WaitSk}.

contains([],_) -> false;
contains([H|T],Elem) -> 
	case H == Elem of
		true -> true;
		false -> contains(T,Elem)
	end.
	
ratingAsterisk(N) -> ratingAsterisk(N,[]).
ratingAsterisk(0,Acc) -> Acc;
ratingAsterisk(N,Acc) -> ratingAsterisk(N-1,[42|Acc]).

filter(Pred,List) -> filter(Pred,List,[]).
filter(_,[],Acc) -> Acc;
filter(Pred,[H|T],Acc) ->
	case Pred(H) of
		true -> filter(Pred,T,Acc++[H]);
		false -> filter(Pred,T,Acc)
	end.

fold(_,[],Acc) -> Acc;
fold(Fun,[H|T],Acc) -> fold(Fun,T,Fun(H,Acc)).
	
check(List,Cat) ->
	{Proc1, Bad1} = handleDup(List),
	Bad2 = filter(fun(X) -> not(contains(Cat,X)) end, Proc1),
	Bad = Bad1++Bad2,
	Bad.
	
handleDup(List) -> handleDup(List,[],[]).
handleDup([],Good,Bad) -> {Good,Bad};
handleDup([H|T],Good,Bad) ->
	case contains(T,H) of
		true ->
			case contains(Bad,H) of
				true -> handleDup(T,Good,Bad);
				false -> handleDup(T,Good,Bad++[H])
			end;
		false ->
			case contains(Bad,H) of
				true -> handleDup(T,Good,Bad);
				false -> handleDup(T,Good++[H],Bad)
			end
		end.